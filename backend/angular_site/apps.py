from django.apps import AppConfig


class AngularSiteConfig(AppConfig):
    name = 'angular_site'
