from django.db import models
from django.contrib.auth.models import User


class Cat(models.Model):
    breeder = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=70, blank=False, default='')
    age = models.IntegerField(blank=False, default=1)
