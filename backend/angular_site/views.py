from django.http import HttpResponse
from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser
from rest_framework import status
from rest_framework.response import Response
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView

from angular_site.models import Cat
from angular_site.serializers import CatSerializer, CreateUserSerializer


class CreateUser(APIView):

    def post(self, request):
        serializer = CreateUserSerializer(data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CatList(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (TokenAuthentication,)

    def get(self, request):
        customers = Cat.objects.filter(breeder=self.request.user)
        customers_serializer = CatSerializer(customers, many=True)
        return JsonResponse(customers_serializer.data, safe=False)

    def post(self, request):
        customer_data = JSONParser().parse(request)
        customer_data['breeder'] = self.request.user.id
        customer_serializer = CatSerializer(data=customer_data)
        if customer_serializer.is_valid():
            customer_serializer.save()
            return JsonResponse(customer_serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(customer_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CatDetail(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (TokenAuthentication,)

    def get(self, request, pk):
        try:
            customer = Cat.objects.get(pk=pk)
        except Cat.DoesNotExist:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)
        customer_serializer = CatSerializer(customer)
        return JsonResponse(customer_serializer.data)

    def put(self, request, pk):
        try:
            customer = Cat.objects.get(pk=pk)
        except Cat.DoesNotExist:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)
        customer_data = JSONParser().parse(request)
        customer_data['breeder'] = self.request.user.id
        customer_serializer = CatSerializer(customer, data=customer_data)
        if customer_serializer.is_valid():
            customer_serializer.save()
            return JsonResponse(customer_serializer.data)
        return JsonResponse(customer_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        try:
            customer = Cat.objects.get(pk=pk)
        except Cat.DoesNotExist:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)
        customer.delete()
        return HttpResponse(status=status.HTTP_204_NO_CONTENT)
