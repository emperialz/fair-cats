from django.conf.urls import url
from angular_site import views
from rest_framework.authtoken.views import ObtainAuthToken

urlpatterns = [
    url(r'^auth/', ObtainAuthToken.as_view()),
    url(r'^cats/$', views.CatList.as_view()),
    url(r'^register/', views.CreateUser.as_view()),
    url(r'^cats/(?P<pk>[0-9]+)$', views.CatDetail.as_view()),
]
